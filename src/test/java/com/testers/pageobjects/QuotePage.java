package com.testers.pageobjects;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.testers.helpclass.CustomMethods;
import com.testers.helpclass.CustomWaits;

public class QuotePage {
	WebDriver driver;
	
	public QuotePage(WebDriver driver) {
		this.driver = driver;
	}
	
	private By buyInsuranceButton= By.cssSelector("#payment");
	
	public QuotePage waitForPageToLoad(){
		new CustomWaits(driver).waitForElementToBeVisible(buyInsuranceButton, 20);
		return this;
	}
	
	public PaymentPage clickBuyInsurance(){
		
		new CustomMethods(driver).mouseHoverAndClick(driver.findElement(buyInsuranceButton));	
		return new PaymentPage(driver).waitForPageToLoad();
				
	}
	
	public QuotePage verifyBuyInsuranceRendering(){
		 Assert.assertTrue(driver.findElement(buyInsuranceButton).isDisplayed());
		 return this;
	}
	
}
