package com.testers.helpclass;

public class DataProvider {
	
	
	private String carType="BMW";
	private String year="2000";
	private String age="40";
	private String gender="female";
	private String state="New South Wales";
	private String email="divyatv2005@gmail.com";
	private String name="Divya Vijayan";
	private String password="divya1234";
	private String cardNumber ="4111111111111111";
	private String cardExpMonth="06";
	private String cardExpYear="2018";
	private String cvv="218";
	 
	 
	public String getCarType() {
		return carType;
	}
	public String getYear() {
		return year;
	}
	public String getAge() {
		return age;
	}
	public String getGender() {
		return gender;
	}
	public String getState() {
		return state;
	}
	public String getEmail() {
		return email;
	}
	public String getName() {
		return name;
	}
	public String getPassword() {
		return password;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public String getCardExpMonth() {
		return cardExpMonth;
	}
	public String getCardExpYear() {
		return cardExpYear;
	}
	public String getCvv() {
		return cvv;
	}

	 
}
