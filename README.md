#### README ####

* This Webdriver project demonstrates design and development of functional test using  page object pattern.
* It's a maven project with testng.xml test suite added as a dependency to pom.xml.File testng.xml has two test classes and input test data configured.Tests can be configured to run on either Firefox or Chrome.

### Features covered ###  

1. Get car quote on http://sydneytesters.herokuapp.com/.
2. Buy Insurance.

### Dependency ###

All the dependency are specified in the pom.xml

### Test Classes ###  

CarPageTests.java and PaymentPageTest.java

### HOW To RUN THE TEST ### 

* Right click pom.xml and select Run As -> Maven test.
* Alternatively, Right click testng.xml (src/main/resources/testng.xml) and 
  select Run As -> TestNG Suite.